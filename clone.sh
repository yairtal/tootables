#!/usr/bin/env bash
file_name=archive
current_time=$(date "+%Y.%m.%d-%H.%M.%S")

tar -zcvf ~/tootables_archive/$file_name.$current_time.tar.gz /var/nodes/tootables_*

mkdir ~/tootables_clone_tmp && cd $_ && rm -rf ~/tootables_clone_tmp/*
git clone git@bitbucket.org:yairtal/tootables.git ~/tootables_clone_tmp
sudo mkdir /var/nodes/tootables_$current_time && sudo mv ~/tootables_clone_tmp/* /var/nodes/tootables_$current_time && rm -rf ~/tootables_clone_tmp

sudo cp -r /etc/letsencrypt/live/tootables.com/* /home/ubuntu/keys/




cd /var/nodes/tootables_$current_time