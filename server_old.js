// Load required modules
var http    = require("http");              // http server core module
var express = require("express");           // web framework external module
var io      = require("socket.io");         // web socket external module
var easyrtc = require("easyrtc");           // EasyRTC external module
var fs = require('fs');
var lex = require('letsencrypt-express');
var path = require('path');
// backend-specific defaults will be passed through
// Note: Since agreeTos is a legal agreement, I would suggest not accepting it by default
var bkDefaults = {
    //webrootPath: path.join(__dirname, '..', 'tests', 'acme-challenge')
    fullchainTpl: '/live/:hostname/fullchain.pem'
    , privkeyTpl: '/live/:hostname/privkey.pem'
    , configDir: path.join(__dirname, '..', 'tests', 'letsencrypt.config')
    , server: LE.stagingServer
};




// Add request module to call XirSys servers
var request = require("request");

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var app = express();
app.use(express.static(__dirname + "/static/"));

// Start Express http server on port 8080
//var webServer = http.createServer(app).listen(80);

var webServer_lex = lex.create({
    configDir: '/etc/letsencrypt'
    , key: fs.readFileSync('/etc/letsencrypt/live/tootables.com/fullchain.pem')
    , cert: fs.readFileSync('/etc/letsencrypt/live/tootables.com/cert.pem')
    , onRequest: app
    , server: require('letsencrypt').productionServerUrl
    , approveRegistration: function (hostname, cb) {
        // check a database or something, get the user
        // show them the agreement that you've already downloaded
        cb(null, {
            domains: ['tootables.com']
            , email: 'yair.tal3@gmail.com'
            , agreeTos: true
        });
    }
}).listen(

    // you can give just the port, or expand out to the full options
    //[{ port: 80, address: 'localhost', onListening: function () { console.log('http://localhost'); } }]
    []

    // you can give just the port, or expand out to the full options
    //, [{ port: 443, address: 'localhost' }]
    , [443]

    // this is pretty much the default onListening handler
    , function onListening() {
        var server = this;
        var protocol = ('requestCert' in server) ? 'https': 'http';
        console.log("Listening at " + protocol + '://localhost:' + this.address().port);
    }
);


//var webServer_lex = lex.create('./letsencrypt.config', app).
//    listen([], [11443, 115001], function () {
//        console.log("ENCRYPT __ALL__ THE DOMAINS!");
//});

//var webServer_http  = webServer_lex.plainServers[0];
var webServer = webServer_lex.tlsServers[0];

// Start Socket.io so it attaches itself to Express server
var socketServer = io.listen(webServer, {
    "log level":1
});

//,
//approveRegistration: function (hostname, cb) {
//    // check a database or something, get the user
//    // show them the agreement that you've already downloaded
//    cb(null, {
//        domains: ['tootables.com']
//        , email: 'yair.tal3@gmail.com'
//        , agreeTos: true
//    });
//}

easyrtc.on("getIceConfig", function(connectionObj, callback) {

    // This object will take in an array of XirSys STUN and TURN servers
    var iceConfig = [];

    request.post('https://service.xirsys.com/ice', {
            form: {
                ident: "ipubb",
                secret: "5baa4afe-4ef3-11e5-991c-be9c64ebd6b2",
                domain: "ipubproduction.amazonaws.com",
                application: "default",
                room: "default",
                secure: 1
            },
            json: true
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                // body.d.iceServers is where the array of ICE servers lives
                iceConfig = body.d.iceServers;
                console.log(iceConfig);
                callback(null, iceConfig);
            }
        });
});

// Start EasyRTC server
var rtc = easyrtc.listen(app, socketServer);
